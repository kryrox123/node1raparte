const clientes =[
  {id:1,nombre:'Juan'},
    {id:2,nombre:'Marcelo'},
    {id:3,nombre:'Luis'}
];
const pagos = [
  {id:1,pago:'1000',moneda:'Bs'},
  {id:2,pago:'1800',moneda:'Bs'}
];

const id=2;

const getCliente = (id) => {
  return new Promise ((resolve,reject)=>{
    const clien = clientes.find(c =>c.id === id);
    if(clien){
      resolve(clien);
    }else{
      reject(`No existe el empleado con el id ${id}`);
    }
  });
};

const getPago = (id) => {
  return new Promise ((resolve,reject)=>{
    const dinero = pagos.find(p=>p.id===id);
    if(dinero){
      resolve(`id:n${dinero.id}\npago: ${dinero.pago} \nmoneda: ${dinero.moneda}`);
    }else{
      reject(`No existe el empleado con el id ${id}`);
    }
  })
}

const getInfoCliente = async (id)=>{
  try{
    const clien = await getCliente(id);
    const dinero = await getPago(id);
    return `id: ${clien.id}\ncliente:${clien.nombre} \n${dinero}`
  }
  catch(ex){
    throw ex;
  }
};

getInfoCliente(id)
.then (msg => console.log(msg))
.catch(err=>console.log(err))