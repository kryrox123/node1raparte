//Estoy importando el paquete file system (fs)
const fs = require('fs');

const crearArchivo = async (base = 9) => {

  try {
    let salida = '';

    console.log('=================');
    console.log(    `Division del ${base}    `);
    console.log('=================');

    for(let i = 1; i<= 100; i++){
      salida += `${base} / ${i} = ${base / i} \n`;
    }
  
    console.log(salida);

    fs.writeFileSync(`division -${base}.txt`, salida);
    return `division -${base}.txt`;

  } catch (error) {
    throw error;
  }
};

//Exportamos el archivo
module.exports = {
  generarArchivo: crearArchivo
}