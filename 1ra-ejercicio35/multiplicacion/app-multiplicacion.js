const argv = require().argv;

const { generarArchivo } = require('./multiplicacion');

generarArchivo(argv.base)
  .then(nombreArchivo => console.log(nombreArchivo, 'creado'))
  .catch(err => console.log(err));
 